# semence-modele

Modèle de donnée permettant de décrire les caractéristique d'une semence, variété, famille, période de semis, etc.
Le but de se document est de répertorier les informations importantes, et de créer des rapprochements entre différentes sources.